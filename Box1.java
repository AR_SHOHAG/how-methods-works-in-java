class Box
{
	void test()
	{
		System.out.println("No Parameter");
	}

	void test(int a)
		{
			System.out.println(a);
		}

	void test(int a, int b)
		{
			System.out.println(a+"  "+b);
		}

	double test(double a)
		{
			return a;

	  	}


}

class Box1
{
	public static void main(String args[])
	{
		Box b= new Box();

		b.test();
		b.test(2);
		b.test(2,3);
		b.test(5.0);

	}
}
